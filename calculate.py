class MyBigNumber:
    def sum_strings(self,stn1, stn2):
        result = ""
        carry = 0
        i = len(stn1) - 1
        j = len(stn2) - 1
    
        while i >= 0 or j >= 0 or carry > 0:
            digit_sum = carry
    
            if i >= 0:
                digit_sum += int(stn1[i])
                i -= 1
    
            if j >= 0:
                digit_sum += int(stn2[j])
                j -= 1
    
            carry = digit_sum // 10
            digit = digit_sum % 10
            result = str(digit) + result
    
        return result

# Ví dụ sử dụng
stn1 = "561"
stn2 = "312"
result = MyBigNumber().sum_strings(stn1, stn2)
print(result)